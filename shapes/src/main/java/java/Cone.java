// Elissar Fadel- 2134398
package java;

public class Cone implements Shape3d {
    //setting up the fields height and radius
    private double height;
    private double radius;
    //constructer to initiate the height and radius
    public Cone(double height, double radius){
        if(height <= 0 || radius <= 0){
            throw new IllegalArgumentException("the height and radius mus be more than 0");
        }
        this.height= height;
        this.radius= radius;
    }
    
    //the methods implemented from shape3d
    // get volume, calculates volume of cone
    // get surface area calculates surface area of cone
    public double getVolume(){
        throw new UnsupportedOperationException("Not written yet");
    }
    public double getSurfaceArea(){
        throw new UnsupportedOperationException("Not written yet");
    }
}
