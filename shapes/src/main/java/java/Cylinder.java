//Hooman Afshari-2134814
package java;

public class Cylinder implements Shape3d {
    private double radius;
    private double height;
//Constructor
    public Cylinder(double radius, double height) {
    //Input validation
        if (radius<0 || height < 0) {
            throw new IllegalArgumentException("Values cannot be less than zero");
        }
        this.height=height;
        this.radius=radius;
    }
    //getVolume method
    public double getVolume() {

        throw new UnsupportedOperationException("Not written yet");
    }
    //getSurface method
    public double getSurfaceArea() {

        throw new UnsupportedOperationException("Not written yet");

    }

}
